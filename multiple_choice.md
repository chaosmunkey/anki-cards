# Multiple Choice Card

This is a custom card where the order for the potential answers to a question can be randomised. This helps to break the problem which could happen where the person using the cards can just remember which answer is correct based on its position in the list.

###### TODO:
* Persistence between changes in the side: At the moment, the front of the card is randomised, whereas the back reverts back to its original ordering.


## Fields
1. Question
2. Answer_0
3. Answer_1
4. Answer_2
5. Answer_3
6. Answer_4
7. Answer_5
8. Answer
9. Extra

Fields 0 -> 5 are the potential answers to the question. Whereas the `answer` field is a comma separated list of which answers are correct.
##### Example Answers
* `2`
* `0,3,5`


## Front
```html
<div>
    <span id="question">{{Question}}</span>
    <br><br>
    <div class="choices" id="choices">
        {{#Answer_0}}

        <div class="choice" id="answer_0">
            <span id="span_0" class="choiceIndex">A:&nbsp;</span>{{Answer_0}}
        </div>
        {{/Answer_0}}
        {{#Answer_1}}
        <div class="choice" id="answer_1">
            <span id="span_1" class="choiceIndex">B:&nbsp;</span>{{Answer_1}}
        </div>
        {{/Answer_1}}
        {{#Answer_2}}
        <div class="choice" id="answer_2">
            <span id="span_2" class="choiceIndex">C:&nbsp;</span>{{Answer_2}}
        </div>
        {{/Answer_2}}
        {{#Answer_3}}
        <div class="choice" id="answer_3">
            <span id="span_3" class="choiceIndex">D:&nbsp;</span>{{Answer_3}}
        </div>
        {{/Answer_3}}
        {{#Answer_4}}
        <div class="choice" id="answer_4">
            <span id="span_4" class="choiceIndex">E:&nbsp;</span>{{Answer_4}}
        </div>
        {{/Answer_4}}
        {{#Answer_5}}
        <div class="choice" id="answer_5">
            <span id="span_5" class="choiceIndex">F:&nbsp;</span>{{Answer_5}}
        </div>
        {{/Answer_5}}
    </div>

    <div id="metadata" style="display:none">
       <span id="answers">{{Answer}}</span>
    </div>
</div>

<script>
    var letters = ["A", "B", "C", "D", "E", "F"];

    backside = document.getElementById("back");
    var answers = document.getElementById("answers").innerHTML.split(",");
    var choicesDiv = document.getElementById("choices");
    var choices = Array.prototype.slice.call(choicesDiv.getElementsByClassName("choice"));

    if (backside === null) {
        for(var i = choices.length - 1; i >= 0; i--) {
            if (answers.indexOf(String(i)) !== -1 ) choices[i].classList.add("correct");
            choicesDiv.removeChild(choices[i]);
        }

        // the shuffling can't be done as a part of the previous loop,
        // it will end messing up the correct answers.
        for (var i = choices.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            [choices[i], choices[j]] = [choices[j], choices[i]];
        }

        choices.forEach(function(choice, index){
            choice.getElementsByClassName("choiceIndex")[0].innerHTML = letters[index]+":&nbsp;";
            choicesDiv.appendChild(choice);
        });
    }
</script>
```

## Back
```html
{{FrontSide}}
<hr>
<div id="back">
    {{Extra}}
</div>



<script>
    var choices = document.getElementsByClassName("choice");
    for(var i = 0; i < choices.length; i++) {
        if (answers.indexOf(String(i)) !== -1 ) {
            choices[i].style.color = "#9CCC65";
            choices[i].style.fontWeight ="bold";
        }
    }
</script>
```
