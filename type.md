# Typing Card

This is one of the new card types with the recent versions of Anki _(or at least I think it is)_.

## Fields
1. Front
2. Back
3. Extra


## Front
```html
{{Front}}

{{type:Back}}
```

## Back
```html
{{Front}}

<hr id="answer">

{{type:Back}}

{{#Extra}}
<hr>
{{Extra}}
{{/Extra}}
```
